#include <fcntl.h>
#include <getopt.h>
#include <unistd.h>
#include <stdint.h>

int64_t slenk(char *a1)
{
  int v1; // eax
  unsigned int v3 = 0; // [rsp+14h] [rbp-4h]

  do
    v1 = v3++;
  while ( a1[v1] );
  return v3;
}


ssize_t domp(char* arg1, char* arg2)
{
    write(2, arg1, ((int64_t)(slenk(arg1) - 1)));
    write(2, arg2, ((int64_t)(slenk(arg2) - 1)));
    return write(2, "\x0A", 1);
}

uint64_t slurp(char* arg1)
{
    char* var_20 = arg1;
    int32_t var_10 = 0;
    uint64_t rax_5;
    while (1)
    {
        if (*(int8_t*)var_20 == 0)
        {
            rax_5 = ((uint64_t)var_10);
            break;
        }
        char* rax_1 = var_20;
        var_20 = &rax_1[1];
        int32_t rax_4 = (((int32_t)*(int8_t*)rax_1) - 0x30);
        if ((rax_4 >= 0 && rax_4 <= 9))
        {
            int32_t rax_8 = (var_10 * 5);
            var_10 = (rax_4 + (rax_8 + rax_8));
            continue;
        }
        rax_5 = 0;
        break;
    }
    return rax_5;
}


int64_t reed(int a1, char *const *a2)
{
  int longind; // [rsp+10h] [rbp-A0h] BYREF
  int v4; // [rsp+14h] [rbp-9Ch]
  int v5; // [rsp+18h] [rbp-98h]
  int v6; // [rsp+1Ch] [rbp-94h]
  struct option longopts; // [rsp+20h] [rbp-90h] BYREF
  const char *v8; // [rsp+40h] [rbp-70h]
  int v9; // [rsp+48h] [rbp-68h]
  int64_t v10; // [rsp+50h] [rbp-60h]
  int v11; // [rsp+58h] [rbp-58h]
  const char *v12; // [rsp+60h] [rbp-50h]
  int v13; // [rsp+68h] [rbp-48h]
  int64_t v14; // [rsp+70h] [rbp-40h]
  int v15; // [rsp+78h] [rbp-38h]
  int64_t v16; // [rsp+80h] [rbp-30h]
  int v17; // [rsp+88h] [rbp-28h]
  int64_t v18; // [rsp+90h] [rbp-20h]
  int v19; // [rsp+98h] [rbp-18h]

  longopts.name = "encrypt";
  longopts.has_arg = 0;
  longopts.flag = 0LL;
  longopts.val = 101;
  v8 = "decrypt";
  v9 = 0;
  v10 = 0LL;
  v11 = 100;
  v12 = "key";
  v13 = 1;
  v14 = 0LL;
  v15 = 107;
  v16 = 0LL;
  v17 = 0;
  v18 = 0LL;
  v19 = 0;
  v4 = 0;
  v5 = 0;
  while ( 1 )
  {
    v6 = getopt_long(a1, a2, "edk:", &longopts, &longind);
    if ( v6 < 0 )
      break;
    if ( v6 == 107 )
    {
      v5 = slurp(optarg);
      if ( v5 <= 0 || v5 > 25 )
      {
        domp("Invalid key: ", optarg);
LABEL_12:
        _exit(1);
      }
    }
    else
    {
      if ( v6 > 107 )
        goto LABEL_12;
      if ( v6 == 100 )
      {
        v4 = 1;
      }
      else
      {
        if ( v6 != 101 )
          goto LABEL_12;
        v4 = 0;
      }
    }
  }
  if ( !v5 )
  {
    write(2, "Key required.\n", 0xEuLL);
    _exit(1);
  }
  if ( v4 )
    return (unsigned int)(26 - v5);
  return (unsigned int)v5;
}


uint64_t rote(int a1, char a2)
{
  int v3; // [rsp+14h] [rbp-4h]

  if ( a2 > 90 || a2 <= 64 )
  {
    if ( a2 > 122 || a2 <= 96 )
      return (uint8_t)a2;
    v3 = 97;
  }
  else
  {
    v3 = 65;
  }
  return (unsigned int)((a2 - v3 + a1) % 26 + v3);
}


void pype(int32_t arg1, int32_t arg2) {
    while (1)
    {
        char var_11;
        if (read(arg1, &var_11, 1) == 0)
        {
            break;
        }
        var_11 = rote(arg2, var_11);
        write(1, &var_11, 1);
    }
}



uint64_t main(int argc,char **argv)

{
  uint32_t uVar1;
  int __fd;
  
  uVar1 = reed(argc,argv);
  if (optind >= argc) {
    pype(0,uVar1);
    return 0;
  } else {
    for (; optind < argc; optind = optind + 1) {
      __fd = open(argv[optind],0);
      if (__fd < 0) {
        domp("Could not open file: ",argv[optind]);
      }
      else {
        pype(__fd,uVar1);
        close(__fd);
      }
    }
    return 0;
  }
}
