use std::env;
use std::io;
use std::ops::Deref;

// wvv-wwww-wv-v^wwww-wv-v-wwww^vvw-vvv-vww^wvv-wvw-vvv-wwv-vvw-wwww-v
const SHRDLU: &str = "?ETIANMSURWDKGOHVF?L?PJBXCYZQ??";

fn enco(key: &Vec<char>, input: &str) -> String {
    let mut out = String::new();
    let mut do_push_space = false;
    let uppercase = input.to_ascii_uppercase();
    let deref = uppercase.deref();
    let chars = deref.chars();
    for ch in chars {
        let chu32 = ch as u32;
        if chu32 == 0x20 {
            out.push('/');
            do_push_space = false;
        } else {
            if do_push_space {
                out.push(' ');
                do_push_space = false;
            }

            if chu32 == 0x3f {
                out.push('\x3f');
            } else {
                let key_deref = key.deref();
                match key_deref.iter().position(|x| {
                                    *x == ch
                                }) {
                    Some(pos) => {
                        let mut vec = Vec::new();
                        do_push_space = true;

                        let mut ctr = pos;
                        while ctr > 0 {
                            if ctr & 1 == 1 {
                                vec.push('.')
                            } else {
                                vec.push('-')
                            }

                            ctr -= 1;
                            ctr >>= 1;
                        }

                        let vec_deref = vec.deref();
                        let vec_iter = vec_deref.iter();
                        let vec_rev = vec_iter.rev();
                        for x in vec_rev {
                            out.push(*x);
                        }
                    },
                    None => {
                        out.push('?');
                    },
                }
            }
        }
    }
    out
}

fn deco(key: &Vec<char>, input: &str) -> String {
    let mut out = String::new();
    let mut ctr = 0;
    let chars = input.chars();
    for ch in chars {
        if ch == '.' {
            ctr = ((ctr + 1) * 2) - 1;
        } else if ch == '-' {
            ctr = (ctr + 1) * 2
        } else if ch == ' ' || ch == '/' {
            if ctr > 0 {
                let key_deref = key.deref();
                let x = key_deref.get(ctr);
                let unwrapped = x.unwrap_or(&'?');
                out.push(*unwrapped);
                ctr = 0;
            }
            if ch == '/' {
                out.push(' ');
            }
        } else {
            out.push('?');
            ctr = 0;
        }
    }

    if ctr > 0 {
        let key_deref = key.deref();
        let x = key_deref.get(ctr);
        let unwrapped = x.unwrap_or(&'?');
        out.push(*unwrapped);
    }

    out
}

fn main() {
    let chars = SHRDLU.chars();
    let chars_vec = chars.collect::<Vec<_>>();
    let args = env::args();
    let args_vec = args.collect::<Vec<_>>();
    let args_len = args_vec.len();

    if args_len != 2 {
        println!("USAGE: ./endeavour -[de]");
    } else {
        let op_fn = if args_vec[1] == "-e" {
            enco
        } else if args_vec[1] == "-d" {
            deco
        } else {
            println!("USAGE: ./endeavour -[de]");
            return;
        };

        let mut input;
        loop {
            input = String::new();
            match io::stdin().read_line(&mut input) {
                Err(_) | Ok(0) => break,
                _ => {
                    let trimmed = input.trim();
                    let trimmed_deref = trimmed.deref();
                    println!("{}", op_fn(&chars_vec, trimmed_deref))
                }
            }
        }
    }
}
