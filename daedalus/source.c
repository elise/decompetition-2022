#include<stdio.h>
#include<stdlib.h>
#include <stdint.h>

/*###########
#.#...H.....#
#.#.####.####
#.....#...#.#
#.######....#
@.#.......#.#
###########*/


int * grid_new(int param_1,int param_2)

{
  int *piVar1;
  void *pvVar2;
  
  piVar1 = (int *)malloc(0x10);
  pvVar2 = calloc((long)(param_1 * param_2),8);
  *(void **)(piVar1 + 2) = pvVar2;
  *piVar1 = param_1;
  piVar1[1] = param_2;
  return piVar1;
}


void * node_new(void)

{
  void *pvVar1;
  
  pvVar1 = malloc(0x10);
  *(void **)pvVar1 = pvVar1;
  *(int *)((long)pvVar1 + 8) = 0;
  return pvVar1;
}

void grid_set(int *param_1,int param_2,int param_3,unsigned long param_4)

{
  if ((param_2 < 0) || (*param_1 <= param_2)) {
                    /* WARNING: Subroutine does not return */
    abort();
  }
  if ((-1 < param_3) && (param_3 < *(param_1 + 1))) {
    *(unsigned long *)(*(long *)(param_1 + 2) + (long)(param_2 + *param_1 * param_3) * 8) = param_4;
    return;
  }
                    /* WARNING: Subroutine does not return */
  abort();
}

unsigned long grid_get(int *param_1,int param_2,int param_3)

{
  unsigned long uVar1;
  
  if ((param_2 < 0) || (*param_1 <= param_2)) {
    uVar1 = 0;
  }
  else if ((param_3 < 0) || (param_1[1] <= param_3)) {
    uVar1 = 0;
  }
  else {
    uVar1 = *(unsigned long *)((long)(param_2 + *param_1 * param_3) * 8 + *(long *)(param_1 + 2));
  }
  return uVar1;
}


int64_t *edge_new(int64_t arg1, int64_t arg2)
{
    int64_t* rax = malloc(0x18);
    *rax = arg1;
    *(rax + 1) = arg2;
    *(uint8_t *)(rax + 2) = 0;
    return rax;
}


void fys(long param_1,int param_2)

{
  int iVar1;
  unsigned long uVar2;
  int iVar3;
  int local_24;
  
  local_24 = param_2;
  while (local_24 != 0) {
    iVar3 = rand();
    iVar1 = local_24 + -1;
    uVar2 = *(unsigned long *)(param_1 + (long)iVar1 * 8);
    *(unsigned long *)(param_1 + (long)iVar1 * 8) =
         *(unsigned long *)(param_1 + (long)(iVar3 % local_24) * 8);
    *(unsigned long *)((long)(iVar3 % local_24) * 8 + param_1) = uVar2;
    local_24 = iVar1;
  }
  return;
}

long fin(long *param_1)
{
  if (param_1 != (long *)*param_1) {
    *param_1 = fin(*param_1);
  }
  return *param_1;
}

unsigned long uni(unsigned long param_1,unsigned long param_2)

{
  long **pplVar1;
  long *plVar2;
  
  pplVar1 = (long **)fin(param_1);
  plVar2 = (long *)fin(param_2);
  if (pplVar1 == (long **)plVar2) {
    return 0;
  }
  else {
    if (*(int *)(pplVar1 + 1) < *(int *)(plVar2 + 1)) {
      *(int *)(pplVar1 + 1) = *(int *)(pplVar1 + 1) + *(int *)(plVar2 + 1);
      *plVar2 = (long)pplVar1;
    }
    else {
      *(int *)(plVar2 + 1) = *(int *)(plVar2 + 1) + *(int *)(pplVar1 + 1);
      *pplVar1 = plVar2;
    }
    return 1;
  }
}



/*int ** plan_new(int param_1,int param_2,int param_3)

{
  unsigned long *puVar1;
  char cVar2;
  int iVar3;
  int **ppiVar4;
  int *piVar5;
  void *__ptr;
  ulong uVar6;
  ulong uVar7;
  ulong *puVar8;
  int local_48;
  int local_44;
  int local_40;
  int local_3c;
  
  ppiVar4 = (int **)malloc(0x18);
  piVar5 = grid_new(param_1,param_2 + -1);
  ppiVar4[2] = piVar5;
  piVar5 = grid_new(param_1 + -1,param_2);
  ppiVar4[1] = piVar5;
  piVar5 = grid_new(param_1,param_2);
  *ppiVar4 = piVar5;
  __ptr = malloc((long)((param_2 * param_1 * 2 - param_1) - param_2) << 3);
  local_48 = 0;
  for (local_44 = 0; local_44 < param_2; local_44 = local_44 + 1) {
    for (local_40 = 0; local_40 < param_1; local_40 = local_40 + 1) {
      uVar6 = node_new();
      grid_set(*ppiVar4,local_40,local_44,uVar6);
      if (local_40 != 0) {
        uVar7 = grid_get(*ppiVar4,local_40 + -1,local_44);
        puVar8 = edge_new(uVar6,uVar7);
        grid_set(ppiVar4[1],local_40 + -1,local_44,(ulong)puVar8);
        *(ulong **)((long)local_48 * 8 + (long)__ptr) = puVar8;
        local_48 = local_48 + 1;
      }
      if (local_44 != 0) {
        uVar7 = grid_get(*ppiVar4,local_40,local_44 + -1);
        puVar8 = edge_new(uVar6,uVar7);
        grid_set(ppiVar4[2],local_40,local_44 + -1,(ulong)puVar8);
        *(ulong **)((long)local_48 * 8 + (long)__ptr) = puVar8;
        local_48 = local_48 + 1;
      }
    }
  }
  fys(__ptr,local_48);
  local_3c = 0;
  do {
    if (local_48 <= local_3c) {
      free(__ptr);
      return ppiVar4;
    }
    puVar1 = *(unsigned long **)((long)__ptr + (long)local_3c * 8);
    cVar2 = uni(*puVar1,puVar1[1]);
    if (cVar2 == '\0') {
      iVar3 = rand();
      if (iVar3 % 100 < param_3) goto LAB_00101818;
    }
    else {
LAB_00101818:
      *(char *)(puVar1 + 2) = 1;
    }
    local_3c = local_3c + 1;
  } while( 1 );
}*/

int64_t* plan_new(int32_t arg1, int32_t arg2, int32_t arg3) {
    int64_t* rax = malloc(0x18);
    rax[2] = grid_new(arg1, (arg2 - 1));
    *(rax + 1) = grid_new((arg1 - 1), arg2);
    *rax = grid_new(arg1, arg2);
    void* rax_16 = malloc(8LL * (int)(2 * arg1 * arg2 - arg1 - arg2));
    int32_t var_48 = 0;
    for (int32_t var_44 = 0; var_44 < arg2; var_44 = (var_44 + 1))
    {
        for (int32_t var_40_1 = 0; var_40_1 < arg1; var_40_1 = (var_40_1 + 1))
        {
            int64_t* rax_18 = node_new();
            grid_set(*(int64_t*)rax, var_40_1, var_44, rax_18);
            if (var_40_1 != 0)
            {
                int64_t* rax_26 = edge_new(rax_18, grid_get(*(int64_t*)rax, (var_40_1 - 1), var_44));
                int32_t* rax_29 = rax[1];
                grid_set(rax_29, (var_40_1 - 1), var_44, rax_26);
                int32_t rax_30 = var_48;
                var_48 = (rax_30 + 1);
                *(int64_t*)((((int64_t)rax_30) << 3) + rax_16) = rax_26;
            }
            if (var_44 != 0)
            {
                int64_t* rax_39 = edge_new(rax_18, grid_get(*(int64_t*)rax, var_40_1, (var_44 - 1)));
                int32_t* rax_42 = rax[2];
                grid_set(rax_42, var_40_1, (var_44 - 1), rax_39);
                int32_t rax_43 = var_48;
                var_48 = (rax_43 + 1);
                *(int64_t*)((((int64_t)rax_43) << 3) + rax_16) = rax_39;
            }
        }
    }
    fys(rax_16, var_48);
    for (int32_t var_3c = 0; var_3c < var_48; var_3c = (var_3c + 1))
    {
        int64_t* rax_54 = *(int64_t*)((char*)rax_16 + (((int64_t)var_3c) << 3));
        char rax_58 = uni(*(int64_t*)rax_54, rax_54[1]);
        if ((rax_58 != 0 || (rax_58 == 0 && arg3 > rand() % 100)))
        {
            rax_54[2] = 1;
        }
    }
    free(rax_16);
    return rax;
}






void plan_out(int **param_1)

{
  int iVar1;
  int iVar2;
  long lVar3;
  long lVar4;
  long lVar5;
  long lVar6;
  int local_4c;
  int local_48;
  int local_44;
  int local_40;
  int local_3c;
  
  iVar1 = (*param_1)[1];
  local_3c = **param_1;
  for (local_4c = 0; local_4c < iVar1; local_4c = local_4c + 1) {
    for (local_48 = 0; local_48 < local_3c; local_48 = local_48 + 1) {
      lVar6 = grid_get(param_1[1],local_48 + -1,local_4c + -1);
      lVar3 = grid_get(param_1[2],local_48 + -1,local_4c + -1);
      lVar4 = grid_get(param_1[1],local_48 + -1,local_4c);
      lVar5 = grid_get(param_1[2],local_48,local_4c + -1);
      local_44 = 0;
      if (lVar6 != 0) {
        local_44 = (int)*(char *)(lVar6 + 0x10);
      }
      if (lVar3 != 0) {
        local_44 = local_44 + *(char *)(lVar3 + 0x10);
      }
      if (lVar4 != 0) {
        local_44 = local_44 + *(char *)(lVar4 + 0x10);
      }
      if (lVar5 != 0) {
        local_44 = local_44 + *(char *)(lVar5 + 0x10);
      }
      if (local_44 == 4) {
        iVar2 = 0x20;
      }
      else {
        iVar2 = 0x2b;
      }
      putchar(iVar2);
      if ((lVar5 == 0) || (*(char *)(lVar5 + 0x10) == '\0')) {
        iVar2 = 0x2d;
      }
      else {
        iVar2 = 0x20;
      }
      putchar(iVar2);
    }
    puts("+");
    for (local_40 = 0; local_40 < local_3c; local_40 = local_40 + 1) {
      lVar6 = grid_get(param_1[1],local_40 + -1,local_4c);
      if ((lVar6 == 0) || (*(char *)(lVar6 + 0x10) == '\0')) {
        iVar2 = 0x7c;
      }
      else {
        iVar2 = 0x20;
      }
      putchar(iVar2);
      putchar(0x20);
    }
    puts("|");
  }
  printf("+ ");
  for (local_3c = local_3c + -2; 0 < local_3c; local_3c = local_3c + -1) {
    printf("+-");
  }
  puts("+ +");
  return;
}



int main(int argc,char **argv)
{
  unsigned long uVar1;
  int local_20;
  int local_1c;
  int local_18;
  uint local_14;
  
  local_20 = 0x14;
  local_1c = 10;
  local_18 = 0x14;
  local_14 = 0x4c;
  if (1 < argc) {
    local_20 = atoi(argv[1]);
  }
  if (2 < argc) {
    local_1c = atoi(argv[2]);
  }
  if (3 < argc) {
    local_18 = atoi(argv[3]);
  }
  if (4 < argc) {
    local_14 = atoi(argv[4]);
  }
  if ((local_20 < 2) || (local_1c < 1)) {
    fwrite("You\'ll never fit a cow in there.\n",1,0x21,stderr);
    return 1;
  }
  else {
    srand(local_14);
    uVar1 = plan_new(local_20,local_1c,local_18);
    plan_out(uVar1);
    return 0;
  }
}