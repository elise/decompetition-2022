    /*
   // B
  // L A
 // I S E
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

int main(int32_t argc, char** argv) {
  int64_t v7 = 0LL;
  int64_t v8 = -1LL;
  if ( argc == 3 )
  {
    v7 = atoll(argv[1]);
    v8 = atoll(argv[2]);
  }
  else if ( argc == 2 )
  {
    v8 = atoll(argv[1]);
  }
  if ( v7 < 0 || v8 < 0 || v7 > v8 )
  {
    std::cerr << "USAGE: ./blaise (range)" << std::endl;
    exit(1);
  }
  for (int64_t i = v7; i <= v8; ++i )
  {
    int64_t v10 = 1LL; // 20
    int64_t v12 = i; // 18
    int64_t v13 = 1LL; // 10
    while ( v12 )
    {
        std::cout << v10 << '\t';
        v10 = (v10 * v12) / v13;
        v13 += 1;
        v12 -= 1;
    }

    std::cout << 1 << std::endl;
  }
  return 0;
}
