use std::{env::args, collections::HashMap, io::{stdout, Write, stdin, Read}};

pub struct State {
    hashmap: HashMap<usize, usize>,
    v1: Vec<u8>,
    v2: Vec<u8>,
    counter: u8
}

impl State {
    pub fn new(bytes: &[u8]) -> Self {
        let mut s = Vec::new();
        let mut hm = HashMap::new();

        for (idx, byte) in bytes.iter().enumerate() {
            if *byte == 0x5b {
               s.push(idx);
            }

            if *byte == 0x5d {
                let popped = s.pop().unwrap();
                hm.insert(idx, popped);
                hm.insert(popped, idx);
            }
        }
        assert_eq!(s.len(), 0); // Line 26
        Self {
            hashmap: hm,
            v1: Vec::new(),
            v2: Vec::new(),
            counter: 0
        }
    }

    pub fn diff(&mut self, x: u8) {
        self.counter += x;
    }

    pub fn dump(&self) -> Option<usize> {
        let counter = self.counter;
        let mut stdout = stdout();
        stdout.write(&[counter]).ok()
    }

    pub fn step(&self, count: usize) {
        
    }

    pub fn read(&mut self) {
        let mut data = [0u8];
        let mut stdin = stdin();
        let _ = stdin.read_exact(&mut data).ok();
        self.counter = data[0];
    }

    pub fn jump(&self, idx: usize, other: usize) -> usize {
        0
    }
}

fn main() {
    let arg = args().nth(1).unwrap();
    let bytes = arg.as_bytes();
    let mut x = 0;
    let mut state = State::new(bytes);

    while x < bytes.len() {
        match bytes[x] {
            b'+' => state.diff(1),
            b',' => state.read(),
            b'-' => state.diff(255),
            b'.' => {
                state.dump();
            },
            b'<' => state.step(1),
            b'>' => state.step(2),
            b'[' => x = state.jump(x, 1),
            b']' => x = state.jump(x, 0),
            _ => break,
        }
        x += 1;
    }
}
