#include <ctype.h>
#include <stdio.h>
#include <stdint.h>

int main() {
    char var_1d = 1;
    while (1)
    {
        int32_t rax_1 = getc(stdin);
        if (rax_1 == 0xffffffff)
        {
            break;
        }
        if ((*(uint16_t**)__ctype_b_loc())[rax_1] & 0x2000)
        {
            putc(rax_1, stdout);
            var_1d = 1;
        }
        else if (var_1d != 0)
        {
            putc(toupper(rax_1), stdout);
            var_1d = 0;
        }
        else
        {
            putc(tolower(rax_1), stdout);
        }
    }
    return 0;
}
