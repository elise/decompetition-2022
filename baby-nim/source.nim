import std/os

when isMainModule:
    if paramCount() != 1:
        echo "USAGE: ./greet name"
        quit(1)

    var name: string = paramStr(1)
    echo "Hi, ", name, "!"