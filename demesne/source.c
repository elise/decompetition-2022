#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>

uint rrrand(int32_t* arg1)
{
    uint local_c;
  
    local_c = *arg1 * 0x343fd + 0x269ec3;
    *arg1 = local_c;
    local_c = local_c >> 0x10;
    if (0x7fff < local_c) {
        local_c = local_c - 0x8000;
    }
    return local_c;
}


int main(int32_t argc, char** argv)
{
    int v4; // [rsp+14h] [rbp-4Ch] BYREF
    int i; // [rsp+18h] [rbp-48h]
    int j; // [rsp+1Ch] [rbp-44h]
    int v7; // [rsp+20h] [rbp-40h]
    int v8; // [rsp+24h] [rbp-3Ch]
    int v9; // [rsp+28h] [rbp-38h]
    int v10; // [rsp+2Ch] [rbp-34h]
    int64_t s[4];

    if (argc < 2) {
        puts("Please provide a seed.");
        return -1;
    }

    v7 = strtol(argv[1], 0LL, 0);
    s[0] = 0;
    s[1] = 0;
    s[2] = 0;
    s[3] = 0;
    for ( i = 0; i <= 15; ++i )
    {
        memset(s, 0, 0x20uLL);
        v4 = v7 ^ i;
        v8 = rrrand(&v4);
        v9 = v8 % 5 + 5;
        for ( j = 0; j < v9; ++j )
        {
            v10 = rrrand(&v4);
            *(j + (uint8_t *)s) = v10 % 26 + 97;
        }
        strcat((char *)s, ".biz");
        puts((const char *)s);
    }
    return 0;
}