use std::ops::Deref;

fn step(input: String) -> String {
    let input_ref = input.deref();
    let chars = input_ref.chars();
    let mut chars_vec = chars.collect::<Vec<_>>();
    let res = chars_vec.pop();
    match res {
        Some(x) => {
            let fmt = {
                format!("{}{}", x, step(chars_vec.into_iter().collect::<String>()))
            };
            fmt.to_string()
        },
        None => {
            "".to_owned()
        },
    }
}

fn main() {
    let arg = std::env::args().nth(1).unwrap_or("what".to_string());
    let step_res = step(arg);
    println!("{}", step_res);
}
