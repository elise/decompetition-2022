#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdlib.h>
#include <signal.h>

uint64_t COUNTER;
int64_t CURRENT;
struct __jmp_buf_tag RESTART;

_Noreturn void chk()
{
    if (CURRENT <= 1)
    {
        printf("%ld", COUNTER);
        fflush(stdout);
        raise(2);
    }
    siglongjmp(&RESTART, 0x16);
}

_Noreturn void pty()
{
    if ((CURRENT & 1) != 0)
    {
        siglongjmp(&RESTART, 0xc);
    }
    siglongjmp(&RESTART, 0xa);
}

_Noreturn void inc() {
    COUNTER += 1;
    CURRENT *= 3;
    CURRENT += 1;
    siglongjmp(&RESTART, 0x15);
    /* no return */
}

_Noreturn void dec()
{
    COUNTER += 1;
    CURRENT /= 2;
    siglongjmp(&RESTART, 0x15);
}

int main(int32_t argc, char** argv) {
    if (argc != 2)
    {
        fwrite("Nein!\n", 1, 6, stderr);
        raise(6);
    }
    COUNTER = 0;
    CURRENT = ((int64_t)atoi(argv[1]));
    if (CURRENT <= 0)
    {
        fwrite("Nein...\n", 1, 8, stderr);
        raise(6);
    }
    signal(0xa, dec);
    signal(0xc, inc);
    signal(0x15, chk);
    signal(0x16, pty);
    pid_t pid = getpid();
    volatile int32_t res = __sigsetjmp(&RESTART, 1);
    if (!res)
        res = 0x15;
    kill((int64_t)pid, (uint64_t)res);
    return 0;
}
