#include <cmath>
#include <iostream>

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "USAGE: ./grade n" << std::endl;
        exit(2);
    }
    int32_t arg1_i32 = atoi(argv[1]);
    int32_t var_14 = 1;
    if (arg1_i32 <= 0) {
        std::cerr << "Don't be so negative." << std::endl;
        exit(2);
    }

    for (int32_t x = std::sqrt(arg1_i32); x > 1; x = x - 1) {
        if (((arg1_i32) % (x)) == 0) {
            var_14 = x + (arg1_i32 / x) + var_14;
        }
    }

    if (var_14 == arg1_i32) {
        std::cout << "Perfect!" << std::endl;
        return 0;
    } else {
        std::cout << "Needs improvement." << std::endl;
        return 1;
    }
}
