#include <stdio.h>
#include <string.h>
#include <stdint.h>

int ctoi(char arg1) {
    return arg1 + -0x30;
}

int check(char* arg1) {
    char x[6] = "";
    char var_5c[4] = "";
    char var_30[8] = "";
    char var_58[6] = "";
    sscanf(arg1, "%5c-%3c-%7c-%5c", x, var_5c, var_30, var_58);

    if (strlen(x) != 5 || strlen(var_5c) != 3 || strlen(var_30) != 7 || strlen(var_58) != 5) {
        return 0xffffffff;
    }

    int32_t var_54;
    int32_t var_50;
    sscanf(x, "%3d%2d", &var_54, &var_50);

    if (var_54 <= 0 || var_54 > 0x16e) {
        return 0xffffffff;
    }

    if (!(var_50 <= 3 || var_50 > 0x5E)) {
        return 0xffffffff;
    }

    if (strcmp(var_5c, "OEM")) {
        return 0xffffffff;
    }

    if ( ctoi(var_30[0])
        || !ctoi(var_30[7])
        || (int)ctoi(var_30[7]) > 8 ) {
        return 0xFFFFFFFFLL;
    }

    int32_t v9 = ctoi(var_30[1]) + ctoi(var_30[2]) + ctoi(var_30[3]) + ctoi(var_30[4]) + ctoi(var_30[5]) + ctoi(var_30[6]);
    if ((v9 % 7))
        return 0xffffffff;
    else
        return 0; 
}


int main(int32_t argc, char** argv) {
    if (argc < 2) {
        puts("No key supplied?");
        return -1;
    } else if (check(argv[1]) == 0xffffffff) {
        puts("Invalid Key :(");
        return -1;
    } else {
        puts("Access Granted!");
        return 0;
    }
}