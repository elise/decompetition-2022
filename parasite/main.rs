use std::collections::HashMap;
use std::env;
use std::io::stdin;
use std::iter::Peekable;
use std::ops::Deref;

// PH?NGLUI?MGLW?NAFH?CTHULHU?R?LYEH?WGAH?NAGL?FHTAGN
const I: &str = "L?LL?F?B?BB?V?M?W?WW?G?GG?K?P?PP?C?X?Z?O?J";
const M: &str = "E?EU?I?IU?T?TU?S?SU?A?AE?AEU?AU?N?H?HT?HTU?HU?R?D?DU?U";
const F: &str = "?L?LL?LG?F?FP?FJ?B?V?VL?VM?VW?VG?VZ?VO?VJ?M?W?WG?G?GG?K?P?C?X?Z?O?J";

fn enco(input: &str) -> String {
    let split_i = I.split('?');
    let collected_split_i = split_i.collect::<Vec<_>>();

    let split_m = M.split('?');
    let collected_split_m = split_m.collect::<Vec<_>>();

    let split_f = F.split('?');
    let collected_split_f = split_f.collect::<Vec<_>>();

    let mut output = String::new();

    let input_chars = input.chars();
    for x in input_chars {
        let some_res = if x >= '\u{AC00}' {
            x <= '\u{D7A3}'
        } else { false };

        if some_res {
            let x_u64 = x as u64;
            let adjusted = x_u64 - 0xac00;

            let indexed_i = collected_split_i[adjusted as usize / 0x24C];
            output += indexed_i;

            let indexed_m = collected_split_m[adjusted as usize % 0x24C / 0x1C];
            output += indexed_m;

            let indexed_f = collected_split_f[adjusted as usize % 0x1C];
            output += indexed_f;
            output += " ";
        } else if x == ' ' {
            output += " ";
        }
    }
    output
}

fn read<T: Iterator<Item = char>>(collected: &HashMap<&str, usize>, peekable_by_ref: &mut Peekable<T>) -> Option<usize> {
    let mut res = None;
    let mut string = String::new();

    let mut res2;
    loop {
        match peekable_by_ref.peek() {
            Some(t) => {
                string.push(*t);
                let output_deref = string.deref();
                match collected.get(output_deref) {
                    Some(opt) => {
                        res = Some(*opt);
                        res2 = res;
                        let _ = peekable_by_ref.next();
                    },
                    _ => {
                        res2 = res;
                        break;
                    },
                }
            },
            _ => {
                res2 = res;
                break;
            },
        }
    }

    res2
}

fn deco(input: &str) -> String {
    let collected_i = I.split('?').enumerate().map(|(lhs, rhs)| {
        (rhs, lhs)
    }).collect::<HashMap<_, _>>();

    let collected_m = M.split('?').enumerate().map(|(lhs, rhs)| {
        (rhs, lhs)
    }).collect::<HashMap<_, _>>();

    let collected_f = F.split('?').enumerate().map(|(lhs, rhs)| {
        (rhs, lhs)
    }).collect::<HashMap<_, _>>();

    let mut out = String::new();

    let uppercase = input.to_ascii_uppercase();
    let uppercase_deref = uppercase.deref();
    let uppercase_chars = uppercase_deref.chars();
    let mut peekable = uppercase_chars.peekable();

    loop {
        let peek = peekable.peek();
        if peek.is_some() == false {
            break;
        }

        let i_res = read(&collected_i, peekable.by_ref());
        let m_res = read(&collected_m, peekable.by_ref());
        let f_res = read(&collected_f, peekable.by_ref());

        if let Some(i_idx) = i_res {
            if let Some(m_idx) = m_res {
                let adjusted_i_idx = 588 * i_idx;
                let adjusted_m_idx = 28 * m_idx;
                let unk_1 = adjusted_i_idx + adjusted_m_idx;
                let unwrapped_f_idx = f_res.unwrap_or(0);
                let unk_2 = unk_1 + unwrapped_f_idx;
                let ch = char::from_u32((unk_2 + 0xAC00) as u32);
                let ch = ch.unwrap();
                out.push(ch);
            }
        }

        let _take_while = peekable.by_ref().take_while(|_unk| false);
        let _ = peekable.next();
        let peek = peekable.peek();
        
        if peek == Some(&' ') {
            out.push(' ');
            let _ = peekable.next();
        }
    }

    out
}

fn main() {
    let arg_string = env::args().nth(1).unwrap_or(String::from("nope"));
    let function = if arg_string == "-e" {
        enco
    } else if arg_string == "-d" {
        deco
    } else {
        println!("USAGE: ./parasite -[de]");
        return;
    };

    let mut string;
    loop {
        string = String::new();
        let stdin = stdin();
        let res = stdin.read_line(&mut string);

        match res {
            Ok(0) | Err(_)  => {
                break;
            },
            _ => {}
        }

        let trimmed = string.deref().trim_end();
        let op_fn_res = function(trimmed);
        println!("{}", op_fn_res);
    }
}
